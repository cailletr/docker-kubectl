FROM busybox as build

ARG VERSION
ADD https://storage.googleapis.com/kubernetes-release/release/$VERSION/bin/linux/amd64/kubectl /tmp
RUN chmod +x /tmp/kubectl


FROM gcr.io/distroless/static

COPY --from=build /tmp/kubectl /usr/local/bin/kubectl 

CMD ["kubectl"]
